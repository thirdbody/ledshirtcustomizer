//
//  ManagementViewController.swift
//  LEDShirt
//
//  Created by Milo Gilad on 12/13/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit
import GoshDarnIt // profanity checker

class ManagementViewController: UIViewController {
    @IBOutlet weak var displayText: UITextField!
    let gameNight = true;
    @IBOutlet weak var gameNightLabel: UILabel!
    

    @IBOutlet weak var goEaglesButton: UIButton!
    @IBOutlet weak var eaglesRockButton: UIButton!
    @IBOutlet weak var letsGoButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if gameNight {
            gameNightLabel.text = "It's game night! Support the NBPS Eagles by tapping the buttons below and displaying some encouraging messages."
            goEaglesButton.isHidden = false;
            eaglesRockButton.isHidden = false;
            letsGoButton.isHidden = false;
        } else {
            goEaglesButton.isHidden = true;
            eaglesRockButton.isHidden = true;
            letsGoButton.isHidden = true;
        }
    }

    func checkAndUpload(text: String) {
        if text.containsProfanity() || text.contains("profanity") {
            let alert = UIAlertController(title: "Cannot upload messages containing profanity.", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in self.displayText.text = ""; self.displayText.resignFirstResponder() }))
            self.present(alert, animated: true)
            return
        }
        
        if let shirt = pairedShirt {
            if uploadText(to: shirt, as: text) {
                let alert = UIAlertController(title: "Uploaded '" + text + "' to LED shirt.", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in self.displayText.text = ""; self.displayText.resignFirstResponder() }))
                self.present(alert, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "You haven't paired a shirt yet!", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in self.displayText.text = ""; self.displayText.resignFirstResponder() }))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func uploadToShirt(_ sender: UIButton) {
        checkAndUpload(text: displayText.text ?? "");
    }
    
    
    @IBAction func uploadGameNight(_ sender: UIButton) {
        checkAndUpload(text: sender.currentTitle ?? "");
    }
}
