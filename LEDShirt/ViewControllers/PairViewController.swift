//
//  PairViewController.swift
//  LEDShirt
//
//  Created by Milo Gilad on 12/13/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit

var pairedShirt: BluetoothDevice? = nil;

class PairViewController: UIViewController {
    @IBOutlet weak var statusLabel: UILabel!
    
    func updateStatus() {
        if let str = pairedShirt?.name {
            statusLabel.text = "You've paired " + str + ".";
        } else {
            statusLabel.text = "No device paired."
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateStatus()
    }
    
    @IBAction func pairShirt(_ sender: Any) {
        let alert = UIAlertController(title: "Select a device to pair.", message: "If you don't see yours, reboot it and try again.", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        for discoveredDevice in discoverDevices() {
            alert.addAction(UIAlertAction(title: discoveredDevice.name, style: .cancel, handler: { action in pairedShirt = discoveredDevice; self.updateStatus() }))
        }

        self.present(alert, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
